# Count Up - Web Komponente

[Matt Boldt](https://github.com/mattboldt) | [Github](https://github.com/mattboldt/typed.js/) | [Doku](https://mattboldt.github.io/typed.js/docs/) | [Demo](https://mattboldt.github.io/typed.js/)

Typed.js is a library that types. Enter in any string, and watch it type at the speed you've set, backspace what it's typed, and begin a new sentence for however many strings you've set.

<hr>

[[_TOC_]]

## Installation

### Npm
```bash
echo @a-team25:registry=https://gitlab.com/api/v4/projects/47253190/packages/npm/ >> .npmrc
```

```bash
npm i @a-team25/at-typed-js
```

### Yarn
```bash
echo \"@a-team25:registry\" \"https://gitlab.com/api/v4/projects/47253190/packages/npm/\" >> .yarnrc
```

```bash
yarn add @a-team25/at-typed-js
```

## Usage

### Import
Die Komponente muss nur in der "Haupt" JavaScript-Datei des Projekts importiert werden. <br>
Nach dem Import steht die Komponente **global** im Projekt zur Verfügung und es muss nichts weiter getan werden.

```javascript
import '@a-team25/at-typed-js'
```

### Attributes

All parameters can be passed as attributes

```html
<at-typed-js
    type-speed="10"
    type-delay="5"
    back-speed="20"
    back-delay="2"
    smart-backspace
    shuffle
    strings='["Typed.js is a ", "JavaScript", " library"]'
    ... 
/>
```

| Name                    | Type    | Description                                                          | Usage                             |
|-------------------------|---------|----------------------------------------------------------------------|-----------------------------------|
| strings                 | String  | strings to be typed                                                  | `strings="Text 1, Text 2"`        |
| strings-element         | String  | ID of element containing string children                             | `strings-element="'myId'"`        |
| type-speed              | Number  | type speed in milliseconds                                           | `type-speed="50"`                 |
| start-delay             | Number  | time before typing starts in milliseconds                            | `start-delay="1000"`              |
| back-speed              | Number  | backspacing speed in milliseconds                                    | `back-speed="10"`                 |
| smart-backspace         | Boolean | only backspace what doesn't match the previous string                | `smart-backspace="true"`          |
| shuffle                 | Boolean | shuffle the strings                                                  | `shuffle="true"`                  |
| back-delay              | Number  | time before backspacing in milliseconds                              | `back-delay="100"`                |
| fade-out                | Boolean | Fade out instead of backspace                                        | `fade-out="true"`                 |
| fade-out-class          | String  | css class for fade animation                                         | `fade-out-class="'fadeOutClass'"` |
| fade-out-delay          | Boolean | fade out delay in milliseconds                                       | `fade-out-delay="true"`           |
| loop                    | Boolean | loop strings                                                         | `loop="true"`                     |
| loop-count              | Number  | amount of loops                                                      | `loop-count="3"`                  |
| show-cursor             | Boolean | show cursor                                                          | `show-cursor="true"`              |
| cursor-char             | String  | character for cursor                                                 | `cursor-char="'_'"`               |
| auto-insert-css         | Boolean | insert CSS for cursor and fadeOut into HTML                          | `auto-insert-css="true"`          |
| attr                    | String  | attribute for typing Ex: input placeholder, value, or just HTML text | `attr="'placeholder'"`            |
| bind-input-focus-events | Boolean | bind to focus and blur if el is text input                           | `bind-input-focus-events="true"`  |
| conten-type             | String  | 'html' or 'null' for plaintext                                       | `content-type="'html'"`           |

### Template
To get started simply add the `at-typed-js` custom element to your `markup` and pass the text, 
which should be typed to the `strings` property. In addition you need to pass an element with the class `typing` 
to the slot, which will be used as a `wrapper`.

#### Minimal setup

```html
<at-typed-js>
    <p>Erster Text</p>
    <p>Zweiter Text</p>
</at-typed-js>
```
The `typing` class also allows you to just animate certain parts of a string:
```html
<at-typed-js type-speed="70" back-speed="30">
    <p>John</p>
    <p>James</p>
</at-typed-js>
```

#### Advanced Setup with Template Tag
The content can be additionally wrapped with my "template" tag. This ensures that the content of the template tag is not displayed once the DOM is loaded. 
It will be rendered only after it has been read by the web component.

```html
<at-typed-js auto-insert-css type-speed="30" back-speed="40" cursor-char="_" strings='["Test 1", "Test 2", "Test 3"]'>
    <template>
        <p>Typed.js is a <strong>JavaScript</strong> library.</p>
        <p>It ^2000 <em>types</em> out sentences.</p>
    </template>
</at-typed-js>
```

#### Examples

```html
<!-- infinite loop -->
<at-typed-js loop>
    <p>awesome</p>
    <p>brilliant</p>
</at-typed-js>

<!-- type pausing with ^1000-->
<at-typed-js>
    <p>This is text ^1000 which gets paused for 1 second</p>
    <p>wow</p>
    <p>interesting</p>
</at-typed-js>

<!-- output html -->
<at-typed-js type-speed="30">
    <p><strong>Bold</strong></p>
    <p><em>em</em></p>
    <p><h4>Headline 4</h4></p>
</at-typed-js>
```

## License

[MIT](http://opensource.org/licenses/MIT)
