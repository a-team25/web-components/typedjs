import {LitElement} from 'lit';
import {customElement, property, state} from 'lit/decorators.js';
import Typed from 'typed.js';

@customElement('adt-typed-js')
export class AdtTypedJs extends LitElement {

    @state()
    private _typedStringSelector = 'typed-strings';

    @state()
    private _typedSelector = 'typing';

    @property({attribute: true, reflect: true, type: Array})
    strings?: [];

    @property({attribute: 'strings-element', reflect: true, type: String})
    stringsElement?: string;

    @property({attribute: 'type-speed', reflect: true, type: Number})
    typeSpeed?: number = 0;

    @property({attribute: 'start-delay', reflect: true, type: Number})
    startDelay?: number = 0;

    @property({attribute: 'back-speed', reflect: true, type: Number})
    backSpeed?: number = 0;

    @property({attribute: 'smart-backspace', reflect: true, type: Boolean})
    smartBackspace?: boolean = false;

    @property({attribute: true, reflect: true, type: Boolean})
    shuffle?: boolean = false;

    @property({attribute: 'back-delay', reflect: true, type: Number})
    backDelay?: number = 700;

    @property({attribute: 'fade-out', reflect: true, type: Boolean})
    fadeOut?: boolean = false;

    @property({attribute: 'fade-out-class', reflect: true, type: String})
    fadeOutClass?: string = '';

    @property({attribute: 'fade-out-delay', reflect: true, type: Number})
    fadeOutDelay?: number = 500;

    @property({attribute: true, reflect: true, type: Boolean})
    loop?: boolean = false;

    @property({attribute: 'loop-count', reflect: true, type: Number})
    loopCount?: number = Infinity;

    @property({attribute: 'show-cursor', reflect: true, type: Boolean})
    hideCursor?: boolean = false;

    @property({attribute: 'cursor-char', reflect: true, type: String})
    cursorChar?: string = "|";

    @property({attribute: 'auto-insert-css', reflect: true, type: Boolean})
    autoInsertCss?: boolean = false;

    @property({attribute: true, reflect: true, type: String})
    attr?: string | undefined;

    @property({attribute: 'bind-input-focus-events', reflect: true, type: Boolean})
    bindInputFocusEvents?: boolean = false;

    @property({attribute: 'content-type', reflect: true, type: String})
    contentType?: string = 'html';

    connectedCallback() {
        const element = document.createElement('span');
        element.setAttribute('id', this._typedSelector);

        const stringsElement = document.createElement('div');
        stringsElement.setAttribute('id', this._typedStringSelector);

        const template = this.querySelector('template');

        this.appendChild(element);

        stringsElement.innerHTML = template?.innerHTML ?? '';
        this.appendChild(stringsElement);

        new Typed(element, {
            stringsElement: stringsElement.innerHTML.length > 0 ? stringsElement : undefined,
            strings: this.strings,
            typeSpeed: this.typeSpeed,
            startDelay: this.startDelay,
            backSpeed: this.backSpeed,
            smartBackspace: this.smartBackspace,
            shuffle: this.shuffle,
            backDelay: this.backDelay,
            fadeOut: this.fadeOut,
            fadeOutDelay: this.fadeOutDelay,
            fadeOutClass: this.fadeOutClass,
            loop: this.loop,
            loopCount: this.loopCount,
            showCursor: !this.hideCursor,
            cursorChar: this.cursorChar,
            autoInsertCss: this.autoInsertCss,
            attr: this.attr,
            bindInputFocusEvents: this.bindInputFocusEvents,
            contentType: this.contentType,
            onBegin: this._fireSimpleEvent.bind({instance: this, event: 'onBegin'}),
            onComplete: this._fireSimpleEvent.bind({instance: this, event: 'onComplete'}),
            preStringTyped: this._fireExtendedEvent.bind({instance: this, event: 'preStringTyped'}),
            onStringTyped: this._fireExtendedEvent.bind({instance: this, event: 'onStringTyped'}),
            onLastStringBackspaced: this._fireSimpleEvent.bind({instance: this, event: 'onLastStringBackspaced'}),
            onTypingPaused: this._fireExtendedEvent.bind({instance: this, event: 'onTypingPaused'}),
            onTypingResumed: this._fireExtendedEvent.bind({instance: this, event: 'onTypingResumed'}),
            onReset: this._fireSimpleEvent.bind({instance: this, event: 'onReset'}),
            onStop: this._fireExtendedEvent.bind({instance: this, event: 'onStop'}),
            onStart: this._fireExtendedEvent.bind({instance: this, event: 'onStart'}),
            onDestroy: this._fireSimpleEvent.bind({instance: this, event: 'onDestroy'}),
        });
    }

    _fireSimpleEvent (self: Typed) {
        this['instance'].dispatchEvent(new CustomEvent(this['event'], {
            bubbles: true,
            cancelable: true,
            detail: self
        }))
    }

    _fireExtendedEvent(arrayPos: number, instance: Typed) {
        this['instance'].dispatchEvent(new CustomEvent(this['event'], {
            bubbles: true,
            cancelable: true,
            detail: {
                arrayPos,
                instance
            }
        }))
    }
}
